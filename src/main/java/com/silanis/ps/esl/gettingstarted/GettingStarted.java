package com.silanis.ps.esl.gettingstarted;

import static com.silanis.esl.sdk.builder.DocumentBuilder.newDocumentWithName;
import static com.silanis.esl.sdk.builder.PackageBuilder.newPackageNamed;
import static com.silanis.esl.sdk.builder.SignatureBuilder.signatureFor;
import static com.silanis.esl.sdk.builder.SignerBuilder.newSignerWithEmail;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.LogManager;

import com.silanis.esl.sdk.DocumentPackage;
import com.silanis.esl.sdk.DocumentType;
import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.PackageId;
import com.silanis.esl.sdk.SessionToken;
 
public class GettingStarted {
 
    public static final String API_URL = "https://sandbox.e-signlive.com/api"; // USE https://apps.e-signlive.com/api FOR PRODUCTION
    private static final String DATE_FORMAT = "EEE, d MMM yyyy HH:mm:ss Z";
 
    public static void main( String[] args ) throws SecurityException, IOException {
    	InputStream fis = GettingStarted.class.getResourceAsStream("/logging.properties");
    	LogManager.getLogManager().readConfiguration(fis);
    	
    	String API_KEY = System.getProperty("API_KEY");
        EslClient eslClient = new EslClient( API_KEY, API_URL );
        
        InputStream documentA = GettingStarted.class.getResourceAsStream("test.pdf");
        InputStream documentB = GettingStarted.class.getResourceAsStream("test.pdf");
        
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        
        // Build the DocumentPackage object
        DocumentPackage documentPackage = newPackageNamed( "Policy " + format.format( new Date() ) )
                .withSigner( newSignerWithEmail( "johnsmith@mailinator.com" )
                        .withCustomId( "Client1" )
                        .withFirstName( "John" )
                        .withLastName( "Smith" ) )
                .withSigner( newSignerWithEmail( "pattygalant@mailinator.com" )
                        .withFirstName( "Patty" )
                        .withLastName( "Galant" ) )
                .withDocument( newDocumentWithName( "Document A" )
                		.fromStream(documentA, DocumentType.PDF)
                        .withSignature( signatureFor( "johnsmith@mailinator.com" )
                                .onPage( 0 )
                                .atPosition( 100, 100 ) ) )
                .withDocument( newDocumentWithName( "Document B" )
                        .fromStream(documentB, DocumentType.PDF)
                        .withSignature( signatureFor( "pattygalant@mailinator.com" )
                                .onPage( 0 )
                                .atPosition( 100, 100 ) ) )
                .build();
 
        // Issue the request to the e-SignLive server to create the DocumentPackage
        PackageId packageId = eslClient.createPackage( documentPackage );
 
        System.out.println( "PackageId: " + packageId );
 
        // Send the package to be signed by the participants
        eslClient.sendPackage( packageId );
 
        String pkgId = packageId.toString();
        // Optionally, get the session token for integrated signing.
        SessionToken sessionToken = eslClient.getSessionService().createSessionToken( pkgId, "Client1" );
    }
}
