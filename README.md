# e-SignLive GettingStarted Sample #

This working e-SignLive sample code refers to the java sample at http://docs.e-signlive.com/doku.php?id=esl:e-signlive_getting_started#creating_your_first_document_package

This project contains a test class for you to run in package

    com.silanis.ps.esl.gettingstarted.GettingStartedTest.java
    
Make sure you set your own eSL API key obtained from your e-SignLive sandbox account in the code prior to executing it.

If you want to package this application as a jar file and run it from the command line, don't forget to set the following argument:

    -DAPI_KEY=<your api key goes here>